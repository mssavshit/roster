<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\CrewMember;
use App\Duty;

class importRoster extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:roster';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Import roster report, extract member details and  duties, add in database';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = app()->call('App\Http\Controllers\DutyController@importRoster');

        if(isset($data['msg']) && $data['msg'] != '')
        {
        	echo $data['msg'];
        }
        else
        {
        	$from = date($data['start_date']);
	        $to = date($data['end_date']);

	        $res = CrewMember::find($data['member_id'])->duties()->whereBetween('duty_date', [$from, $to])->take(10)->get();
        	echo json_encode($res);	
        }

        

        
    }
}
