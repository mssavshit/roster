<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use File;
use DOMDocument;
use DomXPath;
use App\CrewMember;
use App\Duty;

class DutyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function extractHTML($path)
    {
    	$dom = new DOMDocument;
    	$dom->loadHTMLFile($path);
    	$finder = new DomXPath($dom);

    	$dom->preserveWhiteSpace = false;
    	$tables = $dom->getElementsByTagName('table');
    	$rows = $tables->item(0)->getElementsByTagName('tr');
		
		$member_details = $rows->item(3)->getElementsByTagName('td');
    	$member_details = $member_details->item(0)->nodeValue;
    	
    	$member_details = htmlentities($member_details, null, 'utf-8');
		$member_details = str_replace("&nbsp;", " ", $member_details);
		$member_details = html_entity_decode($member_details);

    	$member_details = preg_replace('/\([A-Z0-9-, ]*\)/','', $member_details); 
    	$member_details = preg_replace(['/NAME[\s]*:/','/ID/'], '', $member_details);

    	$member_details = explode(':', $member_details);
    	
    	$member_name = trim($member_details[0]);
    	$member_id = trim($member_details[1]);

    	$nodes = $finder->query("//*[contains(concat(' ', normalize-space(@class), ' '), ' s14 ')]");
    	
    	$member = CrewMember::where('id', '=', $member_id)->first();
		
		if ($member === null) {
			$member = new CrewMember;
	    	$member->id = $member_id;
	    	$member->name = $member_name;
	    	$member->save();
		}
		else
		{
			$member_id = $member->id;
		}

		$lastRow = count($rows)-1;
		$dates = $rows->item($lastRow)->getElementsByTagName('td');
		$dates = htmlentities($dates[0]->nodeValue, null, 'utf-8');
		$dates = explode('&nbsp;', $dates);

		$year = explode('/', $dates[0])[2];
		
		$dutyArr = [];
		$tempArr = [];
    	for($i=5; $i <= 35; $i++)
    	{
    		$duty_details = $rows->item($i)->getElementsByTagName('td');
    		foreach ($duty_details as $key => $value) {
    			
    			$string = htmlentities($value->nodeValue, null, 'utf-8');
				$content = str_replace("&nbsp;", " ", $string);
				$content = html_entity_decode($content);
				
				$tempArr[$key][] = $content;
    		}
    	}
    	
    	$cnt = 0;
    	foreach ($tempArr as $k => $val) {
    		$temp = [];
	    	$duty = [];
	    	$cnt = 0;

			$date = $year.'-'.date('m', strtotime(substr($val[0], 0, 3))).'-'.substr($val[0], 3, 2);
    		$day = substr($val[0], 5, 3);
    		$dutyArr[$k]['date'] = $date;
    		$dutyArr[$k]['day'] = $day;
			$cnt = 1;
    		for($j=1; $j <= 30; $j++)
    		{
    			if($j == 7)
    			{
    				continue;
    			}
    			else if($j < 6)
    			{
    				$temp[] = $val[$j];
    			}
    			else if($j == 6)
    			{
    				if($temp[0] == 'D/O')
    				{
    					$duty[] = ['D/O'];
    				}
    				else
    				{
    					$temp[] = $val[$j];
						$duty[] = $temp;
    				}
					$temp = [];
    			}
	    		else
	    		{
    				if($cnt % 6 == 0)
    				{
    					if($temp[0] == ' ')
    					{
    						$duty[] = ['no-duty'];
    					}
    					else
    					{
    						$duty[] = $temp;
    						$temp = [];
    					}
    					$cnt++;
    				}
    				else
    				{
    					$temp[] = $val[$j];
    					$cnt++;
    				}
	    			$dutyArr[$k]['duties'] = $duty;
	    		}
    		}
    	}
    	
    	$dutyDetail = [];
    	foreach ($dutyArr as $ind => $dataArr) {
    		foreach ($dataArr['duties'] as $in => $dutyInfo) {
    			
    			$dutyDetail = [];
    			$dutyDetail['crew_member_id'] = $member_id;
	    		$dutyDetail['duty_date'] = $dataArr['date'];
	    		$dutyDetail['duty_day'] = $dataArr['day'];

    			if($dutyInfo[0] == 'no-duty')
    			{
    				continue;
    			}
				else if(in_array($dutyInfo[0], ['D/O','ESBY','CSBE','ADTY','INTV']))
				{
					$dutyDetail['duty_type'] = $dutyInfo[0];
					$dutyDetail['flight_number'] = 0;
					$dutyDetail['report_time'] = '';    			
					$dutyDetail['departure_station'] = '';    		
					$dutyDetail['departure_time'] = '';    			
					$dutyDetail['arrival_station'] = '';    			
					$dutyDetail['arrival_time'] = '';
				}
				else
				{
					$dutyDetail['duty_type'] = '';
					$dutyDetail['flight_number'] = $dutyInfo[0];
					$dutyDetail['report_time'] = $dutyInfo[1];    			
					$dutyDetail['departure_station'] = $dutyInfo[3];    		
					$dutyDetail['departure_time'] = $dutyInfo[2];    			
					$dutyDetail['arrival_station'] = $dutyInfo[4];    			
					$dutyDetail['arrival_time'] = isset($dutyInfo[5]) ? $dutyInfo[5] : '' ;		
				}
				$dutyDetailArr[] = $dutyDetail;
    		}
    	}
    	
    	$start_date = $dutyDetailArr[0]['duty_date'];
    	end($dutyDetailArr);
    	$last_key = key($dutyDetailArr);
    	$end_date = $dutyDetailArr[$last_key-1]['duty_date'];

    	$from = date($start_date);
        $to = date($end_date);

        CrewMember::find($member_id)->duties()->whereBetween('duty_date', [$from, $to])->delete();

		if(Duty::insert($dutyDetailArr))
		{
			$resArr['status'] = 'success';
			$resArr['member_id'] = $member_id;
			$resArr['member_name'] = $member_name;
			$resArr['start_date'] = $start_date;
			$resArr['end_date'] = $end_date;

			return $resArr;
		}
		else
		{
			$resArr['status'] = 'faild';
			return $resArr;
		}
    }

    public function importRoster()
    {
    	//$files = FILE::allFiles(storage_path('app/roster_report'));
    	$files = app_path('roster_report');
        if(!is_dir($files))
        {
            $data['msg'] = 'Folder not found';
            return $data;
        }
        $files = FILE::allFiles(app_path('roster_report'));

    	if(sizeof($files) == 0)
    	{
    		$data['msg'] = 'No file found';
    		return $data;
    	}

    	foreach ($files as $key => $file) {
    	
    		$path = $file->getPathname();
    		$ext = strtolower($file->getExtension());
    		switch ($ext) {
    			case 'html':
    				// Extract data from HTML file
    				$data = $this->extractHTML($path);
    				break;
    			case 'pdf':
    				//  Extract data from PDF file
    				break;
    			case 'txt':
    				// Extract data from TEXT file
    				break;
    			case 'xlsx':
    			case 'xls':
    				// Extract data from EXCEL file
    				break;
    			default:
    				$data = 'File format not supported.';
    				break;
    		}
    	}

    	return $data;
    }
}
