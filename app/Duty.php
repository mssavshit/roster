<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Duty extends Model
{
    // Table Name
    protected $table = 'duties';

    // Primary Key
    public $primaryKey = 'id';

    //Timestamps
    public $timestamps = true;  

    public function crewMember()
	{
	    return $this->belongsTo('App\CrewMember');
	}

}
