<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CrewMember extends Model
{
    // Table Name
    protected $table = 'crew_members';

    // Primary Key
    public $primaryKey = 'id';

    //Timestamps
    public $timestamps = true;  

    public function duties()
	{
	    return $this->hasMany('App\Duty');
	}
}
