<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDutiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('duties', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('crew_member_id');
            $table->date('duty_date');
            $table->string('duty_day');
            $table->string('duty_type')->nullable();
            $table->integer('flight_number')->nullable();
            $table->string('report_time')->nullable();
            $table->string('departure_station')->nullable();
            $table->string('departure_time')->nullable();
            $table->string('arrival_station')->nullable();
            $table->string('arrival_time')->nullable();
            $table->timestamp('created_at')->useCurrent();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('duties');
    }
}
